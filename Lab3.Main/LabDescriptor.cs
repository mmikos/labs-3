﻿using Lab3.Contract;
using System;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(ISzcotki);
        public static Type I2 = typeof(Itasma);
        public static Type I3 = typeof(INatrysk);

        public static Type Component = typeof(funkcjonalnosc);

        public static GetInstance GetInstanceOfI1 = funkcjonalnosc.GetSzcotki;
        public static GetInstance GetInstanceOfI2 = funkcjonalnosc.GetTasma;
        public static GetInstance GetInstanceOfI3 = funkcjonalnosc.GetNatrysk;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(dodatkowafunkcjonalnosc);
        public static Type MixinFor = typeof(funkcjonalnosc);

        #endregion
    }
}
