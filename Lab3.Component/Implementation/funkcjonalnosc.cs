﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    public class funkcjonalnosc : Ifunkcjonalnosc
    {
        INatrysk natrysk;
        ISzcotki szcotki;
        Itasma tasma;

        public funkcjonalnosc()
        {
            natrysk = new Natrysk();
            szcotki = new Szcotki();
            tasma = new tasma();
        }

        public void start()
        {
            natrysk.odkrecwode();
            szcotki.szoruj();
            tasma.ciagnij();
        }

        public void stop()
        {
            natrysk.zakrecwode();
            szcotki.stopszoruj();
            tasma.stopciagnij();
        }

        public Itasma Tasma()
        {
            return this.tasma;
        }

        public ISzcotki Szcotki()
        {
            return this.szcotki;
        }

        public INatrysk Natrysk()
        {
            return this.natrysk;
        }

        public static object GetTasma(object tasma)
        {
            return(tasma as funkcjonalnosc).Tasma();
        }

        public static object GetSzcotki(object szcotki)
        {
            return (szcotki as funkcjonalnosc).Szcotki();
        }

        public static object GetNatrysk(object natrysk)
        {
            return (natrysk as funkcjonalnosc).Natrysk();
        }

    }
}
